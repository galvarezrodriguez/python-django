# ######Clases
# class Persona:
# 	def inicializar(self,nombre):#self (this) es un primer parametro que hace referencia a la instancia de la clase
# 		self.nombre=nombre
# 	def obtenerNombre(self):
# 		print(self.nombre)

# persona1 = Persona() #Creando una instancia de la clase persona, un objeto
# persona1.inicializar('Dario')
# persona1.obtenerNombre()

# persona2 = Persona()
# persona2.inicializar('German')
# persona2.obtenerNombre()


# ###### Estructuras

# class Entrada:
# 	pass

# nuevaEntrada = Entrada()
# nuevaEntrada.nombre = 'Bienvenido'
# nuevaEntrada.fecha = '20-04-2013'

# class Animal:
# 	def inicializar(self,nombre,tipo):
# 		self.nombre  = nombre
# 		self.tipo	= tipo

# animal = Animal()
# animal.inicializar('perro','canino')
# print(animal.nombre)
# animal.nombre= 'gato'
# print(animal.nombre)


# ######Herencia
# class Persona:
# 	def hablar(self,texto):
# 		print(texto)

# 	def decir_algo(self):
# 		self.hablar('Decir algo desde padre')


# class Dario(Persona): #Soporta herencia multiple... , otra clase
# 	def decir_hola(self):
# 		self.hablar('Hola Mundo')

# 	def decir_algo(self):
# 		super(Dario, self).decir_algo()

# dario = Dario()
# dario.decir_hola()
# dario.decir_algo()


#######Polimorfismo

# class ClaseA:
# 	def inicializar(self):
# 		self.numero = 1
# 	def avanzar(self):
# 		self.numero+=1
# 		print(self.numero)

# class ClaseB:
# 	def inicializar(self):
# 		self.numero=3
# 	def avanzar(self):
# 		self.numero+=1
# 		print(self.numero)

# instanciaA = ClaseA()
# instanciaA.inicializar()

# instanciaB = ClaseB()
# instanciaB.inicializar()

# def caminar(objeto):
# 	objeto.avanzar()

# caminar(instanciaA)
# caminar(instanciaB)


# #######Name Metodos Magicos
#Creaun un compportamiento especial en las clases
#Permiten controlar las sitaxis de las clases

class Animal:
	def __init__(self,nombre): #No es un contructor
		self.nombre = nombre
	
	#def __new__()
	#def __add__(x) -> Animal + 1 
	#def __call__(x,y) -> Animal(12,23) pasar parametors a la clase
	#def __eq__(x) -> Animal == Otra clase

	def obtener_nombre(self):
		print(self.nombre)

animal = Animal('Minotauro	print(self.numero)')
#animal.inicializar('Minotauro')
animal.obtener_nombre()

