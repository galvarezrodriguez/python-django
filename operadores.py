print(3+3+1+2)
print(-10-5)
print(25%4)
print(3<5)
print(2==2 or 3==2)

#variables en python

edad = 22
nombre = "Dario Alvarez"
apellido = "Rodriguez"
print(edad)
#con + tambien concatena sin espacios 
print("Mi edad es :",edad,"y mi nombre es",nombre,"y la edad doble es ",(edad*2))
print("Mi nombres es %s" %nombre)
print("Mi nombres es %s %s y mi edad es %d y la mitad es %d" %(nombre,apellido,edad,edad/2))

print("hola")

nombre = "Dario"
print("El triple de mi nombre es ",(nombre*3))

#Caracteres de Escape como en php

print("Sacando otra opcion de comillas \"Tal cual\"")
print(" Mi primera linea \n y otra linea nueva")

print("Nombre \t Edad \t Poblacion")
print("German \t22 \tAlvarez")
print("Dario \t23 \tRodriguez")