from appblog.models import Entrada
from appblog.models import Comentario
from django.contrib import admin

admin.site.register(Entrada)
admin.site.register(Comentario)